;(function(context) {

	var tests;
	var debounce;
	var imageLoader;
	var d;
	var $window = $(window);
	
	if(context) {
		debounce = context.debounce;
		tests = context.tests;
	} else {
		debounce = require('./scripts/debounce.js');
		tests = require('./scripts/tests.js');
	}
	
	d = debounce();

	//parallax
	var Parallax = function($el) {
		var self = this;
		this.debounce = debounce();
		this.$el = $el;
		this.layers = $el.children('.parallax-layer');

		$window
			.on('scroll',function() {
				self.debounce.requestProcess(function() { self.onScroll(); })
			})

		//set up
		this.onScroll();
	};

	Parallax.isElInView = function($el) {
		return $window.scrollTop()+window.innerHeight >= $el.position().top;
	}

	Parallax.prototype.onScroll = function() {
		var scrollTop = $window.scrollTop()-this.$el.position().top;
		if(!Parallax.isElInView(this.$el)) { return; }

		this.layers.each(function() {
			var 
				$el = $(this),
				multiplier = $el.data('multiplier'),
				offset = scrollTop*(multiplier);

			$(this).css({
				'webkitTransform':'translate3d(0,'+offset+'px,0)',
				'mozTransform':'translate3d(0,'+offset+'px,0)',
				'msTransform':'translate3d(0,'+offset+'px,0)',
				'transform':'translate3d(0,'+offset+'px,0)',
			});

		});
	};

	$('.parallax').each(function() { (new Parallax($(this))); });
	
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));