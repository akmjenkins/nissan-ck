;(function(context) {

	//load all required scripts	
	require('./scripts/anchors.external.popup.js');
	require('./scripts/lazy.images.js');
	
	//global
	require('./global.js');
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));