;(function(context) {
	
	var $window = $(window);
	
	var faqs = {
	
		getItems: function() {
			return $('.faq-item');
		},
		
		doEllipses: function() {
			this
				.getItems()
				.filter(function() { return !$(this).hasClass('expanded'); })
				.each(function() {
					var 
						$el = $(this),
						p = $('p',$el)[0];
						
					if(p.getBoundingClientRect().height > 170) {
						$el.addClass('truncated');
					} else {
						$el.removeClass('truncated');
					}
				});
		}
	
	};
	
	$window.on('resize load',function() { faqs.doEllipses(); });
	$(document)
		.on('click','.faq-item .expand',function() {
			$(this).closest('.faq-item').toggleClass('expanded');
		});

}(typeof ns !== 'undefined' ? window[ns] : undefined));