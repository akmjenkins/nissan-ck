;(function(context) {
	
	var HTMLMarker;

	var HTMLMarkerClosure = function(opts) {
		HTMLMarker = function(opts) {
			this.opts = $.extend({
				selectedClass: 'selected',
				"class": 'map-marker',
				extraClasses: ''
			},opts);
			
			this.listener = null;
			
			this.markerDiv = null;
			
			if(opts.map) {
				this.setMap(opts.map);
			}
		};
		
		HTMLMarker.prototype = new google.maps.OverlayView();
		
		HTMLMarker.prototype.setData = function(data) {
			this.data = data;
		};
		
		HTMLMarker.prototype.getData = function(data) {
			return this.data || {};
		};
		
		HTMLMarker.prototype.clearData = function(data) {
			this.data = null;
		};
		
		HTMLMarker.prototype.getMarkerDiv = function() {
			if(!this.markerDiv) {
				this.markerDiv = $('<div class="'+this.opts['class']+' ' + this.opts['extraClasses'] + '" title="'+(this.opts.title || '')+'">'+(this.opts.content || '')+'</div>')[0];;
			}
			return this.markerDiv;
		}
		
		HTMLMarker.prototype.onAdd = function() {
			var self = this;
			this.getPanes().floatPane.appendChild(this.getMarkerDiv());
			this.listener = google.maps.event.addDomListener(this.getMarkerDiv(), 'click', function() { google.maps.event.trigger(self, 'click'); });
			if(typeof this.opts.onAdd === 'function') {
				this.opts.onAdd.apply(this);
			}
		};
		
		HTMLMarker.prototype.onRemove = function() {
			this.getMarkerDiv().parentNode.removeChild(this.getMarkerDiv());
			this.markerDiv = null;
			google.maps.event.removeListener(this.listener);
		};
		
		HTMLMarker.prototype.draw = function() {
			var 
				projection = this.getProjection(),
				center = projection.fromLatLngToDivPixel(this.opts.position),
				w = parseInt($(this.getMarkerDiv()).css('width'),10),
				h = parseInt($(this.getMarkerDiv()).css('height'),10);
				
				this.getMarkerDiv().style.top = center.y-(h/2)+'px';
				this.getMarkerDiv().style.left = center.x-(w/2)+'px';
			
		};
		
		HTMLMarker.prototype.setSelected = function(selected) {
			if(selected) {
				$(this.getMarkerDiv()).addClass(this.opts.selectedClass);
			} else {
				$(this.getMarkerDiv()).removeClass(this.opts.selectedClass);
			}
			
			//the div has changed size
			this.draw();
			
		};
		
		HTMLMarker.prototype.isSelected = function() {
			return $(this.getMarkerDiv()).hasClass(this.opts.selectedClass);
		};
		
		HTMLMarker.prototype.getPosition = function() {
			return this.opts.position
		};		

		return new HTMLMarker(opts);
	};

	//CommonJS
	if(typeof module !== 'undefined' && module.exports) {
		module.exports = function(opts) {
			return HTMLMarker ? new HTMLMarker(opts) : HTMLMarkerClosure(opts);
		}
	//CodeKit
	} else if(context) {
		context.HTMLMarker = function(opts) {
			return HTMLMarker ? new HTMLMarker(opts) : HTMLMarkerClosure(opts);
		}
	}	
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));