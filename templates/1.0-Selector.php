<?php $bodyclass = 'home selector-bg'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">

	<div class="hero-content">
		
		<h1 class="hero-content-title">Where will you find yourself?</h1>

		<p>
			Choose your experience. Camp Anywhere.
		</p>

	</div><!-- .hero-content -->

	<div class="home-hero-buttons">
		<a href="#" class="lazybg with-img">
			<img src="../assets/images/peaks.png" alt="peaks">
		</a>
		<a href="#" class="lazybg with-img">
			<img src="../assets/images/parks.png" alt="parks">
		</a>
		<a href="#" class="lazybg with-img">
			<img src="../assets/images/pavement.png" alt="pavement">
		</a>
	</div>

</div><!-- .hero -->

<div class="footer-parallax">
	<div class="parallax-layer parallax-one lazybg immediate" data-multiplier="0" data-src="../assets/images/parallax/peaks/footer-1.png"></div>
	<div class="parallax-layer parallax-two lazybg immediate" data-multiplier="0.1" data-src="../assets/images/parallax/peaks/footer-2.png"></div>	
	<div class="parallax-layer parallax-three lazybg immediate" data-multiplier="0.2" data-src="../assets/images/parallax/peaks/footer-3.png"></div>		
</div>

<?php include('inc/i-footer.php'); ?>