<?php $bodyclass = 'pavement-bg'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">

	<div class="hero-content">
		
		<h1 class="hero-content-title">2015 Nissan Micra</h1>

		<p>
			If you’ve got no trouble admitting that the only bears you’re likely to encounter this 
			May 24th weekend will be on your jar of peanut butter, and if you’re much more likely 
			to be found in slippers than rubber boots, you need to find yourself in a 2015 Nissan Micra. 
		</p>

	</div><!-- .hero-content -->

	<div class="hero-parallax parallax">
		<div class="parallax-layer parallax-one lazybg immediate" data-multiplier="0" data-src="../assets/images/parallax/pavement/layer-1.png"></div>
		<div class="parallax-layer parallax-two lazybg immediate" data-multiplier="0.2" data-src="../assets/images/parallax/pavement/layer-2.png"></div>
		<div class="parallax-layer parallax-three lazybg immediate" data-multiplier="0.4" data-src="../assets/images/parallax/pavement/layer-3.png"></div>
		<div class="parallax-layer parallax-four lazybg immediate" data-multiplier="0.6" data-src="../assets/images/parallax/pavement/layer-4.png"></div>
	</div><!-- .parallax -->

</div><!-- .hero -->

<div class="body">
	
	<div class="sw">
		
		<div class="content-boxes">

			<div class="content-box">
				<div class="hgroup">
					<h2 class="hgroup-title">Find Yourself In Town</h2>
					<span class="hgroup-subtitle">Find adventure in the everyday</span>
				</div><!-- .hgroup -->

				<p>
					What if you got to drive the car you loved, while saving fuel and 
					reducing your carbon footprint? With everything from exterior aerodynamics 
					that slip through the wind, to an advanced 1.6-litre 4-cylinder engine 
					with a reliable, responsive 5-speed manual or 4-speed automatic transmission, 
					the Nissan Micra® will leave less of an impression where it counts - on your wallet and the environment.
				</p>
			</div><!-- .content-box -->			

			<div class="img-box lazybg with-img" data-src="">
				<img src="../assets/images/micra.png" alt="Nissan Micra">
			</div><!-- .img-box -->
			
		</div><!-- .context-boxes -->



	</div><!-- .sw -->

	<div class="banner">
		<div class="sw">
			
			<div class="banner-box">

				<div class="banner-text">
					Finance or Lease for as Low as
				</div>

				<div class="banner-meta">
					<span class="banner-text-rate">
						0<sup>%</sup>
					</span>
					<span class="banner-text-apr">APR on <br> select models</span>
				</div>

			</div>

			<div class="banner-box">

				<div class="banner-text">
					Payments as Low as
				</div>		

				<div class="banner-meta">
					<span class="banner-text-price">$89</span>
					<span class="banner-text-interval">bi-weekly</span>
				</div>

			</div>

		</div><!-- .sw -->
	</div><!-- .banner -->

	<div class="lazybg" data-src="../assets/images/micra-bg.jpg">
		<div class="sw">
			
			<div class="footer-form">

				<h2 class="footer-form-title">As unique and fun as you are.</h2>
				<p>
					To receive a promo code for a free gas card when you test drive a Titan, 
					and to receive more information, leave your information below and we’ll be in touch.
				</p>

				<form action="/" class="body-form full">
					<div class="fieldset">
						<input type="text" name="fname" placeholder="First Name">
						<input type="text" name="lname" placeholder="Last Name">
						<input type="tel" name="phone" placeholder="Phone (optional)">
						<input type="email" name="email" placeholder="E-mail Address">
						<button class="button">Submit</button>
					</div><!-- .fieldset -->
				</form><!-- .body-form -->				
				
			</div><!-- .footer-form -->

		</div><!-- .sw -->
	</div><!-- .lazybg -->

</div><!-- .body -->

<div class="footer-parallax">
	<div class="parallax-layer parallax-one lazybg immediate" data-multiplier="0" data-src="../assets/images/parallax/pavement/footer-1.png"></div>
	<div class="parallax-layer parallax-two lazybg immediate" data-multiplier="0.1" data-src="../assets/images/parallax/pavement/footer-2.png"></div>	
	<div class="parallax-layer parallax-three lazybg immediate" data-multiplier="0.2" data-src="../assets/images/parallax/pavement/footer-3.png"></div>		
</div>

<?php include('inc/i-footer.php'); ?>