<?php $bodyclass = 'peaks-bg'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">

	<div class="hero-content">

		<h1 class="hero-content-title">2015 Nissan Titan</h1>

		<p>
			If your hands are more likely to be sticky from sap than sampling artisinal honeys at 
			the farmer’s market, and if your ideal camping experience is more likely to include 
			Four Wheel Drive than four lane highways, you need to find yourself in a 2015 Nissan 
			Titan this May 24th Weekend.
		</p>

	</div><!-- .hero-content -->

	<div class="hero-parallax parallax">
		<div class="parallax-layer parallax-one lazybg immediate" data-multiplier="0" data-src="../assets/images/parallax/peaks/layer-1.png"></div>
		<div class="parallax-layer parallax-two lazybg immediate" data-multiplier="0.2" data-src="../assets/images/parallax/peaks/layer-2.png"></div>
		<div class="parallax-layer parallax-three lazybg immediate" data-multiplier="0.4" data-src="../assets/images/parallax/peaks/layer-3.png"></div>
		<div class="parallax-layer parallax-four lazybg immediate" data-multiplier="0.6" data-src="../assets/images/parallax/peaks/layer-4.png"></div>
		<div class="parallax-layer parallax-five lazybg immediate" data-multiplier="0.8" data-src="../assets/images/parallax/peaks/layer-5.png"></div>
	</div><!-- .parallax -->

</div><!-- .hero -->

<div class="body">
	
	<div class="sw">
		
		<div class="content-boxes">

			<div class="content-box">
				<div class="hgroup">
					<h2 class="hgroup-title">Find Yourself Off Road</h2>
					<span class="hgroup-subtitle">Find yourself camping in a Titan</span>
				</div><!-- .hgroup -->

				<p>
					It’s easy in the Nissan Titan.® Put your foot down on a powerful standard V8 
					engine that hits the road with up to 9400 lbs. of towing capacity. 1 And a fully 
					boxed ladder frame and standard 4x4 handle just about any job on the list. 
					While Titan&reg;’s cabin and bed give you plenty of room. 2015 Nissan Titan.&reg; No 
					matter how epic your plans, it’s ready
				</p>
			</div><!-- .content-box -->			

			<div class="img-box lazybg with-img">
				<img src="../assets/images/titan.png" alt="Nissan Titan">
			</div><!-- .img-box -->
			
		</div><!-- .context-boxes -->



	</div><!-- .sw -->

	<div class="banner">
		<div class="sw">
			
			<div class="banner-box">

				<div class="banner-text">
					Finance or Lease for as Low as
				</div>

				<div class="banner-meta">
					<span class="banner-text-rate">
						0<sup>%</sup>
					</span>
					<span class="banner-text-apr">APR on <br> select models</span>
				</div>

			</div>

			<div class="banner-box">

				<div class="banner-text">
					Payments as Low as
				</div>		

				<div class="banner-meta">
					<span class="banner-text-price">$220</span>
					<span class="banner-text-interval">bi-weekly</span>
				</div>

			</div>

		</div><!-- .sw -->
	</div><!-- .banner -->

	<div class="lazybg" data-src="../assets/images/titan-bg.jpg">
		<div class="sw">
			
			<div class="footer-form">

				<h2 class="footer-form-title">Don't Pay A Load For Your Payload</h2>
				<p>
					To receive a promo code for a free gas card when you test drive a Titan, 
					and to receive more information, leave your information below and we’ll be in touch.
				</p>

				<form action="/" class="body-form full">
					<div class="fieldset">
						<input type="text" name="fname" placeholder="First Name">
						<input type="text" name="lname" placeholder="Last Name">
						<input type="tel" name="phone" placeholder="Phone (optional)">
						<input type="email" name="email" placeholder="E-mail Address">
						<button class="button">Submit</button>
					</div><!-- .fieldset -->
				</form><!-- .body-form -->				
				
			</div><!-- .footer-form -->

		</div><!-- .sw -->
	</div><!-- .lazybg -->

</div><!-- .body -->

<div class="footer-parallax">
	<div class="parallax-layer parallax-one lazybg immediate" data-multiplier="0" data-src="../assets/images/parallax/peaks/footer-1.png"></div>
	<div class="parallax-layer parallax-two lazybg immediate" data-multiplier="0.1" data-src="../assets/images/parallax/peaks/footer-2.png"></div>	
	<div class="parallax-layer parallax-three lazybg immediate" data-multiplier="0.2" data-src="../assets/images/parallax/peaks/footer-3.png"></div>		
</div>

<?php include('inc/i-footer.php'); ?>