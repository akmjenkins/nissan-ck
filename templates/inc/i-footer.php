			<footer>
				
				<div class="copyright">
					<div class="sw">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">O'Neill Nissan</a>. All Rights Reserved.</li>
							<li><a href="#">Legal</a></li>
							<li><a href="#">O'Neill Motors</a></li>
						</ul>
						
						<?php include('i-social.php'); ?>
					</div><!-- .sw -->
				</div><!-- .copyright -->
				
			</footer><!-- .footer -->

		</div><!-- .page-wrapper -->

		<script>
			var templateJS = {
				templateURL: 'http://dev.me/nissan-ck',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>

		<script src="../assets/js/min/main-min.js"></script>
	</body>
</html>