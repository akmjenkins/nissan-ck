<!doctype html>
<html lang="en">

	<head>
		<title>O'Neill Nissan</title>
		<meta charset="utf-8">
		
		<!-- jQuery -->
		<script src="../bower_components/jquery/dist/jquery.min.js"></script>
		
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,initial-scale=1.0">
		
		<link rel="stylesheet" href="../assets/css/style.css?<?php echo time(); ?>">		
		
	</head>
	<body class="<?php echo $bodyclass; ?>">
		
		<header>
			<div class="sw">
				<a href="#" class="header-logo lazybg with-img">
					<img src="../assets/images/nissan-oneill.svg" alt="O'Neill Nissan">
				</a>

				<?php include('i-social.php'); ?>
			</div><!-- .sw -->
		</header>
	
		<div class="page-wrapper">